//------------------------------------------------------------
// Author: 烟雨迷离半世殇
// Mail: 1778139321@qq.com
// Data: 2019年5月17日 20:57:44
//------------------------------------------------------------

using System;
using Sirenix.OdinInspector;

namespace ETModel
{
    public class SkillBaseNodeData
    {
        [LabelText("此节点ID为")]
        public long NodeID;
    }
}