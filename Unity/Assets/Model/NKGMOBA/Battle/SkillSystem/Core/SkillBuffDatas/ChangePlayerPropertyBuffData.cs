//------------------------------------------------------------
// Author: 烟雨迷离半世殇
// Mail: 1778139321@qq.com
// Data: 2019年10月2日 12:17:56
//------------------------------------------------------------

using Sirenix.OdinInspector;

namespace ETModel
{
    public class ChangePlayerPropertyBuffData: BuffDataBase
    {
        /// <summary>
        /// 将要被添加的值
        /// </summary>
        [LabelText("将要被添加的值")]
        public float theValueWillBeAdded = 0;
    }
}