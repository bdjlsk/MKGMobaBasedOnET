/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace ETHotfix
{
	public static partial class FUIPackage
	{
		public const string FUI5v5Map = "FUI5v5Map";
		public const string FUI5v5Map_HpProcessBar = "ui://FUI5v5Map/HpProcessBar";
		public const string FUI5v5Map_OtherBar = "ui://FUI5v5Map/OtherBar";
		public const string FUI5v5Map_SkillSmallProBar = "ui://FUI5v5Map/SkillSmallProBar";
		public const string FUI5v5Map_GoldenToShopBtn = "ui://FUI5v5Map/GoldenToShopBtn";
		public const string FUI5v5Map_BuffProBar = "ui://FUI5v5Map/BuffProBar";
		public const string FUI5v5Map_SkillProBar = "ui://FUI5v5Map/SkillProBar";
		public const string FUI5v5Map_ProgressBar1 = "ui://FUI5v5Map/ProgressBar1";
		public const string FUI5v5Map_Avatar = "ui://FUI5v5Map/Avatar";
		public const string FUI5v5Map_Skill = "ui://FUI5v5Map/Skill";
		public const string FUI5v5Map_UIHeroHead_16 = "ui://FUI5v5Map/UIHeroHead_16";
		public const string FUI5v5Map_RedBar = "ui://FUI5v5Map/RedBar";
		public const string FUI5v5Map_BlueBar = "ui://FUI5v5Map/BlueBar";
		public const string FUI5v5Map_Golden = "ui://FUI5v5Map/Golden";
		public const string FUI5v5Map_Exp = "ui://FUI5v5Map/Exp";
		public const string FUI5v5Map_Back = "ui://FUI5v5Map/Back";
		public const string FUI5v5Map_SmallMap = "ui://FUI5v5Map/SmallMap";
		public const string FUI5v5Map_Clarity_HUDAtlas_8 = "ui://FUI5v5Map/Clarity_HUDAtlas_8";
		public const string FUI5v5Map_QQJieTu20190629224514 = "ui://FUI5v5Map/QQ截图20190629224514";
		public const string FUI5v5Map_QQJieTu20190629231944 = "ui://FUI5v5Map/QQ截图20190629231944";
		public const string FUI5v5Map_QQJieTu20190629232108 = "ui://FUI5v5Map/QQ截图20190629232108";
		public const string FUI5v5Map_Clarity_HUDAtlas_103 = "ui://FUI5v5Map/Clarity_HUDAtlas_103";
		public const string FUI5v5Map_ReplayAtlas_118 = "ui://FUI5v5Map/ReplayAtlas_118";
		public const string FUI5v5Map_PerksAtlas_11 = "ui://FUI5v5Map/PerksAtlas_11";
		public const string FUI5v5Map_PerksAtlas_15 = "ui://FUI5v5Map/PerksAtlas_15";
		public const string FUI5v5Map_PerksAtlas_16 = "ui://FUI5v5Map/PerksAtlas_16";
		public const string FUI5v5Map_PerksAtlas_17 = "ui://FUI5v5Map/PerksAtlas_17";
		public const string FUI5v5Map_PerksAtlas_18 = "ui://FUI5v5Map/PerksAtlas_18";
		public const string FUI5v5Map_PerksAtlas_19 = "ui://FUI5v5Map/PerksAtlas_19";
		public const string FUI5v5Map_PerksAtlas_20 = "ui://FUI5v5Map/PerksAtlas_20";
		public const string FUI5v5Map_PerksAtlas_21 = "ui://FUI5v5Map/PerksAtlas_21";
		public const string FUI5v5Map_PerksAtlas_22 = "ui://FUI5v5Map/PerksAtlas_22";
		public const string FUI5v5Map_PerksAtlas_23 = "ui://FUI5v5Map/PerksAtlas_23";
		public const string FUI5v5Map_PerksAtlas_24 = "ui://FUI5v5Map/PerksAtlas_24";
		public const string FUI5v5Map_PerksAtlas_25 = "ui://FUI5v5Map/PerksAtlas_25";
		public const string FUI5v5Map_PerksAtlas_27 = "ui://FUI5v5Map/PerksAtlas_27";
		public const string FUI5v5Map_Janna_Tailwind = "ui://FUI5v5Map/Janna_Tailwind";
		public const string FUI5v5Map_FUI5V5Map = "ui://FUI5v5Map/FUI5V5Map";
	}
}