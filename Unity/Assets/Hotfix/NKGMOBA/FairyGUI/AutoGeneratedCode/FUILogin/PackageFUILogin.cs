/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace ETHotfix
{
	public static partial class FUIPackage
	{
		public const string FUILogin = "FUILogin";
		public const string FUILogin_ToTestScene = "ui://FUILogin/ToTestScene";
		public const string FUILogin_RegistBtn = "ui://FUILogin/RegistBtn";
		public const string FUILogin_loginBtn = "ui://FUILogin/loginBtn";
		public const string FUILogin_Login = "ui://FUILogin/Login";
		public const string FUILogin_UI_LoginBtn = "ui://FUILogin/UI_LoginBtn";
		public const string FUILogin_UI_LoginBtn_ONpressed = "ui://FUILogin/UI_LoginBtn_ONpressed";
		public const string FUILogin_UI_LoginInput = "ui://FUILogin/UI_LoginInput";
		public const string FUILogin_banzi_da = "ui://FUILogin/banzi_da";
		public const string FUILogin_FUILogin = "ui://FUILogin/FUILogin";
	}
}