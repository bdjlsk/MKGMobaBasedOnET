/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace ETHotfix
{
	public static partial class FUIPackage
	{
		public const string FUIHeadBar = "FUIHeadBar";
		public const string FUIHeadBar_HeadBar_HP = "ui://FUIHeadBar/HeadBar_HP";
		public const string FUIHeadBar_BarBlack = "ui://FUIHeadBar/BarBlack";
		public const string FUIHeadBar_UIMajorCity_23 = "ui://FUIHeadBar/UIMajorCity_23";
		public const string FUIHeadBar_UIMajorCity_6 = "ui://FUIHeadBar/UIMajorCity_6";
		public const string FUIHeadBar_UIMajorCity_14 = "ui://FUIHeadBar/UIMajorCity_14";
		public const string FUIHeadBar_UIMajorCity_9 = "ui://FUIHeadBar/UIMajorCity_9";
		public const string FUIHeadBar_HeadBar = "ui://FUIHeadBar/HeadBar";
	}
}