/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace ETHotfix
{
	public static partial class FUIPackage
	{
		public const string FUILobby = "FUILobby";
		public const string FUILobby_BottomBtn = "ui://FUILobby/BottomBtn";
		public const string FUILobby_ActityHead = "ui://FUILobby/ActityHead";
		public const string FUILobby_BattleBtn = "ui://FUILobby/BattleBtn";
		public const string FUILobby_LobbyBck = "ui://FUILobby/LobbyBck";
		public const string FUILobby_Bottom = "ui://FUILobby/Bottom";
		public const string FUILobby_activity = "ui://FUILobby/activity";
		public const string FUILobby_BottomNormal = "ui://FUILobby/BottomNormal";
		public const string FUILobby_BottomPressed = "ui://FUILobby/BottomPressed";
		public const string FUILobby_TopRight = "ui://FUILobby/TopRight";
		public const string FUILobby_gem = "ui://FUILobby/gem";
		public const string FUILobby_gemself = "ui://FUILobby/gemself";
		public const string FUILobby_golden = "ui://FUILobby/golden";
		public const string FUILobby_pointhead = "ui://FUILobby/pointhead";
		public const string FUILobby_pointback = "ui://FUILobby/pointback";
		public const string FUILobby_mail = "ui://FUILobby/mail";
		public const string FUILobby_setting = "ui://FUILobby/setting";
		public const string FUILobby_UIPublic_61 = "ui://FUILobby/UIPublic_61";
		public const string FUILobby_Dungeon = "ui://FUILobby/Dungeon";
		public const string FUILobby_SeriousPVP = "ui://FUILobby/SeriousPVP";
		public const string FUILobby_NomalPVP = "ui://FUILobby/NomalPVP";
		public const string FUILobby_UIHeroHead_1 = "ui://FUILobby/UIHeroHead_1";
		public const string FUILobby_UIMajorCity_12 = "ui://FUILobby/UIMajorCity_12";
		public const string FUILobby_FUILobby = "ui://FUILobby/FUILobby";
	}
}